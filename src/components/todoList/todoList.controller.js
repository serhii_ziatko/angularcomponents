


export default class TodoController {
	$onInit() {
    this.count = 0;
		this.isShow = true;
    this.todoWidgets = [
      {'idWidget': this.counter(), 'listWidget':[
          { 'id': this.counter(), 'title': 'Lorem ipsum.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit amet.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum.', 'isComplite': false },
      ]},{'idWidget': this.counter(), 'listWidget':[
          { 'id': this.counter(), 'title': 'Lorem ipsum.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit amet.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor sit.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum dolor.', 'isComplite': false },
          { 'id': this.counter(), 'title': 'Lorem ipsum.', 'isComplite': false },
      ]}
    ];
	}

  counter() { return this.count++; }

  addWidget(e) {
    e.preventDefault();
    if( this.todoWidgets.length < 3 ){
      this.todoWidgets.push({'idWidget': this.counter(), 'listWidget': [] });
      if( this.todoWidgets.length === 3 ){
        this.isShowBtn = false;
      }
    }
  }
  removeWidget(e, id){
    e.preventDefault();
    var indexWidget = this.todoWidgets.findIndex( function(item) {
      return  item.idWidget === id ;
    });
    this.todoWidgets.splice(indexWidget, 1);
    this.isShowBtn = true;
  }

}