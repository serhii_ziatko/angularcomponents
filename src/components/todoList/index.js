import ng from 'angular';

import TodoListComponent from './todoList.component';
import Todo from '../todo';

export default ng.module('app.components.todoList', [Todo])
    .component('todoList', TodoListComponent)
    .name;