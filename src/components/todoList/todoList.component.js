import template from './todoList.html';
import controller from './todoList.controller';

export default {
    template,
    controller,
    bindings: {
        widget: '<',
    }
};