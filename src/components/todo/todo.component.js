import template from './todo.html';
import controller from './todo.controller';

export default {
    template,
    controller,
    bindings : {
        widget: "<",
    }
};