import ng from 'angular';

import TodoComponent from './todo.component';
import TodoItem from '../todoItem';

export default ng.module('app.components.todo', [TodoItem])
    .component('todo', TodoComponent)
    .name;