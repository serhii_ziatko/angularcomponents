export default class TodoService {
    constructor($http){
        'ngInject';
        this.$http = $http;
        this.URL = 'http://localhost.3000';
    }
    list(){
        return this.$http.get(`${this.URL}/todos`).then(result => result.data);
    }
    detail( id ){
        return this.$http.get(`${this.URL}/todos/${id}`).then(result => result.data);
    }
    add( data ){
        return this.$http.post(`${this.URL}/todos/`, data).then(result => result.data);
    }
    update( todo ){
        return this.$http.put(`${this.URL}/todos/${todo.id}`, todo).then(result => result.data);
    }
    remove( id ){
        return this.$http.put(`${this.URL}/todos/${id}`);
    }
    save( data ){
        if (data.id){
            return this.update(data);
        }
        return this.add(data);
    }
}