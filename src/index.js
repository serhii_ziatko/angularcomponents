import 'jquery';
import '../node_modules/bootstrap/dist/css/bootstrap-theme.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min';
import './css/main.css';

import TodoList from './components/todoList';

(function(ng) { 
    
    ng.module( 'app', [TodoList]);
})(angular);